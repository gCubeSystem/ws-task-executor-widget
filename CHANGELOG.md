# Changelog for ws-task-executor-widget

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v1.0.1] -  2024-07-17

- Updated `workspace-task-executor-library` dependency lower range [#27747#note-20]

## [v1.0.0] - 2021-01-18

#### Bug Fixes

[#20457] Just including patched library


## [v0.3.0]  2020-07-29

[#19724] Migrated to git/jenkins


## [v0.2.0]  2020-04-10

[Task #17349] Migrate ws-task-executor components to SHUB

[Incident #17506] Bug fixing


## [v0.1.0]  2018-07-30

[Bug #12269] Fixing key parameter for type FILE

first release


